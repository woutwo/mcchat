/** Login Event
 *
 * When a user sends the package 'login'
 * @data.hash = The hash from the user, @data.pass = The password for joining root
 *
 */
socket.on('login', function(data){

    //Parse password
    var pass = data.pass;
    if(data.pass === undefined)
    {
        pass = "";
    }

    //Check if password is null
    if(channels[0].lock != pass)
    {
        socket.emit("exception", {"reason" : "Je probeerde Root te joinen met een fout wachtwoord."});
        socket.disconnect();
        return;
    }
    //Check if the user sends a hash
    if( data.hash === undefined )
    {
        socket.emit("exception", {"reason" : "Er is geen hash meegestuurd."});
        socket.disconnect();
        return ;
    }

    // @TODO: Permissies vanaf de server regelen door de group-id's mee te sturen
    //Sends a request to check if the login is valid
    var req = http.request({host: 'minecraftforum.nl', port: 80, path: '/chat/check.php?hash=' + data.hash, method: 'GET'}, function(res){

        res.setEncoding('utf8');
        //The request gave us data =D
        res.on('data', function(chunk)
        {
            //Parse the JSON
            var parsedChunk = JSON.parse(chunk);

            if(banned.indexOf(parsedChunk.id) != -1) {
                socket.emit("exception", {"reason" : "Je bent gebanned."});
                socket.disconnect();
                return;
            }

            if( parsedChunk.status === false) //The hash is wrong
            {
                socket.emit("exception", {"reason": "Je stuurde een foute hash: " + parsedChunk.m + "."});
                return;
            }

            var userList = {};
            Object.each(users, function(v, k)
            {
                //Check if the user is the same user
                if(v.id == parsedChunk.id)
                {
                    v.socket.emit("exception", {"reason": "Je bent ingelogd met een andere verbinding."});
                    v.socket.disconnect();
                }else{

                    // Gebruiker niet vanished?
                    if(vanished.indexOf(v.id) === -1){

                        //console.log(vanished, v.name+" ("+v.id+"): VANISHED");
                        userList[v.id] = { "name" : v.name, "channel" : v.channel, "permGroup": {"name" : undefined, "color" : v.color } };
                    }
                }
            });

            var channelList = {};
            Object.each(channels, function(v, k){

                channelList[k] = { "name" : v.name, "parent" : v.parent, "locked": v.isLocked()};
            });

            log("logs/info.log", parsedChunk.name + " joined with ID " + parsedChunk.id + " and with socket-ID " + socket.id);

            new User(socket, parsedChunk.name, parsedChunk.id, parsedChunk.groups);

            socket.emit("login", {"users" : userList, "channels" : channelList, "info": {"name" : parsedChunk.name, "id": parsedChunk.id, "permGroup": {"name" : undefined, "color" : users[socket.id].color } } } );

            // Stuur de MOTD mee naar de gebruiker
            var motd = config.motd;
            motd = motd.replace("%user%", parsedChunk.name);
            motd = motd.replace("%name%", config.name);
            motd = motd.replace("%port%", config.portChat);
            motd = motd.replace("%version%", versionNumber);

            socket.emit("chat", {"userId": 0, "m": motd});

            // Is deze gebruiker is momenteel vanished?
            if(vanished.indexOf(parsedChunk.id) !== -1){

                socket.emit("exception", {"reason" : "WAARSCHUWING: JE BENT MOMENTEEL ONTZICHTBAAR EN KUNT DUS NIET CHATTEN"});
            }else{

                socket.broadcast.emit("addUser", { "id" : parsedChunk.id , "name" : parsedChunk.name, "permGroup": {"name" : undefined, "color" : users[socket.id].color } } );
            }


            channels[0].joinChannel(socket.id);

        });
    }).on("exception", function(exception)
        {
            socket.emit("exception", {"reason" : "Er is iets fout gegaan met het controleren van je hash."});
            socket.disconnect();
            console.log('Error:' + exception.message);
        });

    req.end();

});
