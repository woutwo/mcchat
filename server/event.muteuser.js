/** Mute Event
 *
 * When a user sends the package 'mute'
 * @data.id = the id of the user
 *
 */
socket.on('mute', function(data)
{

    if(!users[socket.id].hasPermission("mute"))
    {
        socket.emit("exception", {"reason" : "Je hebt geen permissie voor dat commando."});
        return;
    }

    if(data.id === undefined)
    {
        socket.emit("exception", {"reason" : "Het user-id is niet meegestuurd"});
        return ;
    }

    Object.each(users, function(v, k){
        if(v.id == data.id){
            console.log("Muted: "+users[k].name);
            muted.push(data.id);
            log("logs/info.log",users[socket.id].name + " muted " + users[k].name + " (" + data.id + ")");
            socket.emit("exception", {"reason" : "De user is gemute."});
            v.socket.emit("exception", {"reason" : "Je bent gemute."});
            return;
        }

    });

});