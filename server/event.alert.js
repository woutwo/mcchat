/** Alert Event
 *
 * When a user sends the package 'alert'
 * @data.m = the message
 *
 */
socket.on('alert', function(data)
{
    if(users[socket.id].getChannel().owner != users[socket.id].name) {
        if(!users[socket.id].hasPermission("lock"))
        {
            socket.emit("exception", {"reason" : "Je hebt geen permissie voor dat commando."});
            return;
        }
    }

    //Is the user in a channel according to the user class
    if( users[ socket.id ].channel === undefined)
    {
        socket.emit("exception", {"reason" : "Je bent niet in een kanaal, maar hoort er wel in een te zijn."});
        socket.disconnect();
        return;
    }

    //Check if the channel exists
    if( channels[users[ socket.id ].channel] === undefined)
    {
        socket.emit("exception", {"reason" : "Het kaaal waarin jij zit bestaat niet meer."});
        socket.disconnect();
        return;
    }

    //Check if the users is in the channel
    if( channels[users[ socket.id ].channel].userList[socket.id] === undefined)
    {
        socket.emit("exception", {"reason" : "Je zit in een kanaal, maar je hoort niet in dat kanaal te zijn."});
        socket.disconnect();
        return;
    }

    //Check if the message is undefined
    if(data.m === undefined)
    {
        socket.emit("exception", {"reason" : "Er is geen bericht meegestuurd."});
        return;
    }

    //Check if the message isn't empty
    if(data.m.length === 0)
    {
        socket.emit("exception", {"reason" : "Het meegestuurde bericht is leeg."});
        return;
    }

    log("logs/info.log", "(" + users[socket.id].getChannel().name + ":" + users[socket.id].getChannel().id + ")" + users[socket.id].name + " alerts " + data.m);

    Object.each( channels[users[socket.id].channel].userList, function(v, k) //Loop trough everyone in channel
    {
        if(users[k] === undefined) { return; }

        users[k].socket.emit('alert', {'userId': users[ socket.id].id, 'm': data.m}); //Send message
    });

});