/** Chat Event
 *
 * When a user sends the package 'chat'
 * @data.m = the message
 *
 */
socket.on('chat', function(data){

    if( users[socket.id] === undefined)
    {
        var foundUser = false;
        console.log(socket.id + " said something but wasn't in the list. This could cause problems.");

        //Check if he still might be in the list
        Object.each(users, function(v, k)
        {
            if(v.socket.id === socket.id)
            {
                users[socket.id] = v;
                users[socket.id].socket = socket;

                console.log(socket.id + " was still found in the list by the name " + v.name);
                foundUser = true;
            }
        });

        if(foundUser === false)
        {
            socket.emit("exception", {"reason" : "Je staat niet in de userlijst, maar hoort er wel in te staan."});
            socket.disconnect();
            return;
        }
    }

    if(muted.indexOf(users[socket.id].id) !== -1) {
        socket.emit("exception", {"reason" : "Je bent gemuted."});
        return;
    }

    //Is the user in a channel according to the user class
    if( users[ socket.id ].channel === undefined)
    {
        socket.emit("exception", {"reason" : "Je zit niet in een kanaal, maar hoort er wel in een te zijn."});
        socket.disconnect();
        return;
    }

    //Check if the channel exists
    if( channels[users[ socket.id ].channel] === undefined)
    {
        socket.emit("exception", {"reason" : "Het kanaal waarin jij zit bestaat niet meer."});
        socket.disconnect();
        return;
    }

    //Check if the users is in the channel
    if( channels[users[ socket.id ].channel].userList[socket.id] === undefined)
    {
        socket.emit("exception", {"reason" : "Je zit in een kanaal, maar je hoort niet in dat kanaal te zijn."});
        socket.disconnect();
        return;
    }

    //Check if the message is undefined
    if(data.m === undefined)
    {
        socket.emit("exception", {"reason" : "Er is geen bericht meegestuurd."});
        return;
    }

    //Check spamming
    if(users[socket.id].canSend === false)
    {
        socket.emit("exception", {"reason" : "Je mag niet zo snel achter elkaar posten!."});
        return;
    }

    //Check if the message isn't empty
    if(data.m.length === 0)
    {
        socket.emit("exception", {"reason" : "Het meegestuurde bericht is leeg."});
        return;
    }

    /*d = new Date();
     utc = d.getTime() + (d.getTimezoneOffset() * 60000);
     nd = new Date(utc + (3600000*2));
     if(nd.getHours() >= 22 || nd.getHours() <= 5) {
     socket.emit("exception", {"reason" : "Je mag alleen chatten van 6:00 tot 22:00." });
     socket.disconnect();
     return;
     }*/

    // GEBRUIKER IS AL VANISHED
    if(vanished.indexOf(users[socket.id].id) !== -1){

        if(data.m === "/vanish"){

            // Verwijderd gebruiker uit vanished-lijst
            vanished.splice(vanished.indexOf(users[socket.id].id), 1);

            socket.emit("exception", {"reason" : "JE BENT NU WEER ZICHTBAAR!"});

            var userList = {};
            Object.each(users, function(v, k)
            {
                userList[v.id] = { "name" : v.name, "channel" : v.channel, "permGroup": {"name" : undefined, "color" : v.color } };
            });

            var channelList = {};
            Object.each(channels, function(v, k){

                channelList[k] = { "name" : v.name, "parent" : v.parent, "locked": v.isLocked()};
            });

            socket.emit("login", {"users" : userList, "channels" : channelList, "info": {"name" : users[socket.id].name, "id": users[socket.id].id, "permGroup": {"name" : undefined, "color" : users[socket.id].color } } } );
            socket.broadcast.emit("addUser", { "id" : users[socket.id].id , "name" : users[socket.id].name, "permGroup": {"name" : undefined, "color" : users[socket.id].color } } );

            delete channels[users[socket.id].channel].userList[users[socket.id].id];
            channels[users[socket.id].channel].joinChannel(socket.id);

            return;
        }

        socket.emit("exception", {"reason" : "JE BENT ONTZICHTBAAR EN KUNT DUS NIET CHATTEN /vanish"});
        return;
    }else{
        if(data.m === "/vanish"){

            if(users[socket.id].hasPermission("vanish")){

                // Voeg gebruiker toe aan de lijst
                vanished.push(users[socket.id].id);
                socket.emit("exception", {"reason" : "JE BENT NU ONTZICHTBAAR EN KUNT NIET CHATTEN!"});

                io.sockets.emit("leaveChannel", {"channelId" : users[socket.id].getChannel().id, "userId": users[socket.id].id } );
                io.sockets.emit("removeUser", {"id" : users[socket.id].id});
                return;
            }

        }
    }

    console.log(data.m);

    log("logs/info.log", "(" + users[socket.id].getChannel().name + ":" + users[socket.id].getChannel().id + ")" + users[socket.id].name + ": " + data.m);

    Object.each( channels[users[socket.id].channel].userList, function(v, k) //Loop trough everyone in channel
    {
        if(data.m.length === 0){ return; }
        if(users[k] === undefined) { return; }

        if(users[socket.id].hasPermission("unlimitedLength")) {
            users[k].socket.emit('chat', {'userId': users[ socket.id].id, 'm': data.m, 'color': users[ socket.id].color}); //Send message
        } else {
            if(data.m.length <= 149) {
                users[k].socket.emit('chat', {'userId': users[ socket.id].id, 'm': data.m, 'color': users[ socket.id].color}); //Send message
            } else {
                users[k].socket.emit('chat', {'userId': users[ socket.id].id, 'm': data.m.substring(0,149), 'color': users[ socket.id].color}); //Send message
            }
            //users[ socket.id].canSend = false;
            //setTimeout(function (){users[ socket.id].canSend = true;}, 1000);
        }
    });


});