/** Report Event
 *
 * When a user sends the package 'report'
 * @data.m = the report
 *
 */
socket.on('report', function(data)
{
    //Check if the message is undefined
    if(data.m === undefined)
    {
        socket.emit("exception", {"reason" : "Je hebt geen uitleg gegeven bij het report!"});
        return;
    }

    //Check if the message isn't empty
    if(data.m.length === 0)
    {
        socket.emit("exception", {"reason" : "Het meegestuurde report is leeg!"});
        return;
    }

    log("logs/report.log", users[socket.id].name + " reported: " + data.m);
    socket.emit("exception", {"reason" : "Je report is gepost!"});
    return;
});