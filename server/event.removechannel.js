/** RemoveChannel Event
 *
 * When a user sends the package 'removeChannel'
 * @data.id = the id of the channel
 *
 */
socket.on('removeChannel', function(data)
{

    if(!users[socket.id].hasPermission("removeChannel"))
    {
        socket.emit("exception", {"reason" : "Je hebt geen permissie voor dat commando."});
        return;
    }

    if(data.id === undefined)
    {
        socket.emit("exception", {"reason" : "Het kanaal-id is niet meegestuurd"});
        return ;
    }

    //Check if parent exists
    if(channels[data.id] === undefined)
    {
        socket.emit("exception", {"reason" : "Het kanaal is niet gevonden"});
        return;
    }

    channels[data.id].owner = "";
    log("logs/info.log", users[socket.id].name + " removed channel " + channels[data.id].name + ":" + data.id);

    channels[data.id].removeChannel();

});