Channel = function(iName, iParent, password, temp)
{
    this.id = nextChannelId;
    this.name = iName;
    this.userList = {};
    this.parent = iParent;
    this.lock = (password === undefined ? "" : password);
    this.temp = (temp === undefined ? false : temp);
    this.owner;

    //Add channel to array
    channels[this.id] = this;

    //Raise the next channel Id
    nextChannelId++;

    //Send to everyone that this channel has been created
    io.sockets.emit('addChannel', {'id': this.id , 'name': this.name, 'parent': this.parent});

    /** channel.joinChannel
     *
     * User joines the channel
     * @iSocketId = The socketId from the user
     *
     */

    this.joinChannel = function(iSocketId)
    {
        // Is deze gebruiker is momenteel vanished?
        if(vanished.indexOf(users[iSocketId.id]) === -1){

            io.sockets.emit("joinChannel", {"channelId" : this.id, "userId": users[iSocketId].id } );
        }

        this.userList [ iSocketId ] = users[iSocketId].id;
    };

    /** channel.isLocked
     *
     * Check if channel is locked
     *
     *
     */

    this.isLocked = function()
    {
        if(this.lock === "")
        {
            return false;
        }
        return true;
    };

    /** channel.leaveChannel
     *
     * User leaves the channel
     * @iSocketId = The socketId from the user
     *
     */

    this.leaveChannel = function(iSocketId)
    {
        // Is deze gebruiker is momenteel vanished?
        if(vanished.indexOf(users[iSocketId].id) === -1){

            io.sockets.emit("leaveChannel", {"channelId" : this.id, "userId": users[iSocketId].id } );
        }

        delete this.userList[ iSocketId ];

        if(JSON.stringify(this.userList).length == 2 && this.temp === true) //If no one is in the channel and it's a temp-channel
        {
            io.sockets.emit('removeChannel', {'id' : this.id });
            delete channels[this.id];
        }
    };

    /** channel.isIndirectParent
     *
     * Checks if the channel is a parent of id
     * @iParent = the Id of the parent
     *
     */

    this.isIndirectParent = function(iParent)
    {
        var currentParent = this.parent;

        while(true){


            if(currentParent == iParent){ //It's the same

                return true;
            }else{
                if(currentParent == -1){ //We are back at root

                    return false;
                }
            }
            currentParent = channels[currentParent].parent;
        }
    };

    /** channel.removeChannel
     *
     * Delete this channel
     *
     *
     */

    this.removeChannel = function(iSocketId)
    {
        if( this.parent == -1){ return 0; } //It has no parent, so it can't be deleted
        var id = this.id;

        Object.each(channels, function(v, k){ //Loop trough channels

            if(v.isIndirectParent(id)){ //This channel is a parent of id

                v.removeChannel();
            }
        });

        var obj = this;

        Object.each(this.userList, function(v, k){ //Loop trough all the users

            obj.leaveChannel( users[k].socket.id); //Remove the user from this channel
            channels[obj.parent].joinChannel( users[k].socket.id); //Let the user join the parent
        });

        io.sockets.emit('removeChannel', {'id' : this.id }); //Emit that the channel is deleted
        delete channels[this.id];
    };

};