User = function(iSocket, iName, iId, iGroups)
{
    this.name       = iName;
    this.socket     = iSocket;
    this.channel    = 0;
    this.id         = iId;
    this.color      = undefined;
    this.canSend    = true;
    this.groups     = iGroups;
    this.permission = 3;

    //Add user to array
    users[ iSocket.id ] = this;

    // Check permissies
    if(iGroups.indexOf('1') !== -1)this.permission = 0; // Beheerder

    if(iGroups.indexOf('2') !== -1)this.permission = 1; // Globale Moderator
    if(iGroups.indexOf('3') !== -1)this.permission = 1; // Moderator
    if(iGroups.indexOf('13') !== -1)this.permission = 1; // Lokale Moderator
    if(iGroups.indexOf('24') !== -1)this.permission = 1; // Chat Moderator

    if(iGroups.indexOf('12') !== -1)this.permission = 2; // Erelid
    if(iGroups.indexOf('17') !== -1)this.permission = 2; // Super Erelid

    this.color = config.permissions[this.permission].color;

    this.getChannel = function()
    {
        return channels[this.channel];
    };

    this.hasPermission = function(node)
    {
        if(config.permissions[this.permission] === undefined){

            return false;
        }

        if(config.permissions[this.permission].perms === undefined){

            return false;
        }

        var perm = config.permissions[this.permission].perms.indexOf(node);
        if(perm !== -1){

            return true;
        }

        return false;
    };
};