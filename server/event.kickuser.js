/** Kick Event
 *
 * When a user When a user sends the package 'kick'
 * @data.id = the Id of the user
 *
 */
socket.on('kick', function(data)
{
    if(users[socket.id].getChannel().owner != users[socket.id].name) {
        if(!users[socket.id].hasPermission("kick"))
        {
            socket.emit("exception", {"reason" : "Je hebt geen permissie voor dat commando."});
            return;
        }
    }

    //Check if data.id is sended
    if(data.id === undefined)
    {
        socket.emit("exception", {"reason" : "Het user-id is niet meegestuurd."});
        return ;
    }

    Object.each(users, function(v, k){
        if(users[k].hasPermission('kick')) {
            if(v.id == data.id){
                socket.emit("exception", {"reason" : "Je kan geen beheerder of moderator kicken!"});
                return;
            }
        }

        if(users[k].getChannel().owner === undefined && !users[socket.id].hasPermission('kick'))
        {
            if(v.id == data.id){
                socket.emit("exception", {"reason" : "De user zit niet in een kanaal waar jij kan kicken!"});
                return ;
            }
        }

        if(users[socket.id].hasPermission('kick')) {
            if(v.id == data.id){
                log("logs/info.log",users[socket.id].name + " kicked " + users[k].name);
                v.socket.emit("exception", {"reason" : "Je bent gekicked."});
                v.socket.disconnect();
                return;
            }
        }

        if(users[k].getChannel().owner == users[socket.id].name) {
            if(v.id == data.id){
                if(users[k].getChannel().owner == users[k].name) {
                    socket.emit("exception", {"reason" : "Je kan niet jezelf kicken!"});
                    return;
                }
                log("logs/info.log", users[socket.id].name + " kicked " + users[k].name + "to Root:");

                users[k].getChannel().leaveChannel(k);
                channels[0].joinChannel(k);

                users[k].channel = data.channel;
                log("logs/info.log","Channel admin command: " + users[socket.id].name + " kicked " + users[k].name);
                return;
            }
        }

    });

});