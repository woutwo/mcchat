/** Disconnect Event
 *
 * When a user disconnects
 *
 */

// @TODO: Spookgebruikers goed verwijderen / cleanup
socket.on('disconnect', function(data)
{
    var user = users[socket.id];

    //Check if user exists
    if(users[socket.id] === undefined)
    {
        var foundUser = false;

        log("logs/info.log", socket.id + " disconnected but wasn't in the list. This could cause problems.");
        console.log(socket.id + " disconnected but wasn't in the list. This could cause problems.");

        //Check if he still might be in the list
        Object.each(users, function(v, k)
        {
            if(v.socket.id === socket.id)
            {
                user = v;
                log("logs/info.log", socket.id + " was still found in the list by the name " + v.name);
                console.log(socket.id + " was still found in the list by the name " + v.name);
                foundUser = true;
            }
        });

        if(foundUser === false)
        {
            return;
        }
    }

    var channel = user.getChannel();

    //Check if channel exists
    if(user.getChannel() === undefined)
    {
        var foundChannel = false;
        log("logs/info.log", users[socket.id].name + " disconnected but wasn't in a channel. This could cause problems.");
        console.log(users[socket.id].name + " disconnected but wasn't in a channel. This could cause problems.");

        //Check if he might stil be in a list
        Object.each(channels, function(v1, k1)
        {
            Object.each(v1.userList, function(v2, k2)
            {
                if(k2 === user.socket.id)
                {
                    log("logs/info.log", users[user.socket.id].name + "was still found in channel " + v1.name + "and the problem is solved");
                    console.log(users[user.socket.id].name + "was still found in channel " + v1.name + "and the problem is solved");
                    channel = v1;
                    foundChannel = true;
                }
            });
        });

        if(foundChannel === true)
        {
            users[socket.id].getChannel().leaveChannel(socket.id);
            log("logs/info.log", users[socket.id].name + " left");
            io.sockets.emit('text', {'text': users[ socket.id].name + ' heeft de chat verlaten.'}); //Send message
        }

    }

    // Is deze gebruiker is momenteel vanished?
    if(vanished.indexOf(users[socket.id].id)  === -1){
        users[socket.id].getChannel().leaveChannel(socket.id);
        io.sockets.emit("removeUser", {"id" : users[user.socket.id].id });
        io.sockets.emit('message', {'text': users[ socket.id].name + ' heeft de chat verlaten.'}); //Send message
    }

    delete users[socket.id];

});
