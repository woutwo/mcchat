/** Me Event
 *
 * When a user sends the package 'me'
 * @data.m = the message
 *
 */
socket.on('me', function(data){

    if(muted.indexOf(users[socket.id].id) != -1) {
        socket.emit("exception", {"reason" : "Je bent gemuted."});
        return;
    }
    log("logs/info.log", "(" + users[socket.id].getChannel().name + ":" + users[socket.id].getChannel().id + ")" + users[socket.id].name + ": /ME: " + data.m);

    Object.each( channels[users[socket.id].channel].userList, function(v, k) //Loop trough everyone in channel
    {
        if(data.m.length === 0){ return; }
        if(users[k] === undefined) { return; }

        if(data.m.length <= 149) {
            users[k].socket.emit('me', {'userId': users[ socket.id].id, 'm': data.m}); //Send message
        } else {
            users[k].socket.emit('me', {'userId': users[ socket.id].id, 'm': data.m.substring(0,149)}); //Send message
        }
        return;
    });
});