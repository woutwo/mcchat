/** Announce Event
 *
 * When a user sends the package 'chat'
 * @data.m = the message
 *
 */
socket.on('announce', function(data)
{
    if(!users[socket.id].hasPermission("announce"))
    {
        socket.emit("exception", {"reason" : "Je hebt geen permissie voor dat commando."});
        return;
    }

    //Check if the message is undefined
    if(data.m === undefined)
    {
        socket.emit("exception", {"reason" : "Er is geen bericht meegestuurd."});
        return;
    }

    //Check if the message isn't empty
    if(data.m.length === 0)
    {
        socket.emit("exception", {"reason" : "Het meegestuurde bericht is leeg."});
        return;
    }

    log("logs/info.log", users[socket.id].name + " announces " + data.m);

    io.sockets.emit('announce', {'userId': users[ socket.id].id, 'm': data.m}); //Send message
});