/** Unban Event
 *
 * When a user sends the package 'ban'
 * @data.id = the id of the user
 *
 */
socket.on('unban', function(data)
{

    if(!users[socket.id].hasPermission("ban"))
    {
        socket.emit("exception", {"reason" : "Je hebt geen permissie voor dat commando."});
        return;
    }

    if(data.id === undefined)
    {
        socket.emit("exception", {"reason" : "Het user-id is niet meegestuurd"});
        return ;
    }

    Object.each(users, function(v, k){
        //if(v.id == data.id){
        if(banned.indexOf(data.id) != -1) {
            var index = banned.indexOf(data.id);
            banned.splice(index, 1);
            log("logs/info.log",users[socket.id].name + " unbanned " + data.id);
            console.log("Unbanned: "+data.id);
            socket.emit("exception", {"reason" : "De user is unbanned."});
            return;
        } else { socket.emit("exception", {"reason" : "De user staat niet in de banlijst"}); return; }
        //} else { socket.emit("exception", {"reason" : "De ids zijn niet hetzelfde?"+v.id+" | "+data.id}); return; }

    });

});