/** MoveUser Event
 *
 * When a user sends the package 'changeChannel'
 * @data.channel = The Id of the channel, @data.id = the Id of the user
 *
 */
socket.on('moveUser', function(data)
{
    if(!users[socket.id].hasPermission("move"))
    {
        socket.emit("exception", {"reason" : "Je hebt geen permissie voor dat commando."});
        return;
    }

    //Check if data.id is undefined
    if(data.id === undefined)
    {
        socket.emit("exception", {"reason" : "Het gebruiker-id is niet meegestuurd."});
        return;
    }

    //Check if the channel is undefined
    if(data.channel === undefined)
    {
        socket.emit("exception", {"reason" : "Het kanaal-id is niet meegestuurd."});
        return ;
    }

    //Check if channel exists
    if(channels[data.channel] === undefined)
    {
        socket.emit("exception", {"reason" : "Het kanaal is niet gevonden."});
        return ;
    }

    Object.each(users, function(v, k){

        // The Id of the user is the same
        if(v.id == data.id){
            log("logs/info.log", users[socket.id].name + " moved " + users[k].name + "to" + channels[data.channel].name + ":" + data.channel);

            users[k].getChannel().leaveChannel(k);
            channels[data.channel].joinChannel(k);

            users[k].channel = data.channel;
            return;
        }
    });
});