/** ChangeChannel Event
 *
 * When a user sends the package 'changeChannel'
 * @data.id = The Id of the channel, @data.pass = The password
 *
 */
socket.on("changeChannel", function(data)
{
    //Check if the id of the channel is undefined
    if(data.id === undefined)
    {
        socket.emit("exception", {"reason" : "Het kanaal-id is niet meegestuurd."});
        return ;
    }

    //Is the user in a channel according to the user class
    if( users[ socket.id ].channel === undefined)
    {
        socket.emit("exception", {"reason" : "Je bent niet in een kanaal, maar hoort er wel in een te zijn."});
        socket.disconnect();
        return;
    }

    //Check if the channel exists
    if( channels[users[ socket.id ].channel] === undefined)
    {
        socket.emit("exception", {"reason" : "Het kanaal waarin jij zit bestaat niet meer"});
        socket.disconnect();
        return;
    }

    //Check if the users is in the channel
    if( channels[users[ socket.id ].channel].userList[socket.id] === undefined)
    {
        socket.emit("exception", {"reason" : "Je zit in een kanaal, maar je hoort niet in dat kanaal te zijn."});
        socket.disconnect();
        return;
    }

    //The channel is not found
    if( channels[data.id] === undefined)
    {
        socket.emit("exception", {"reason" : "Het kanaal is niet gevonden."});
        return;
    }

    //The same channel
    if(channels[data.id] === users[socket.id].getChannel())
    {
        return;
    }

    //Set password
    var pass = "";
    if(data.pass !== undefined)
    {
        pass = data.pass;
    }

    //Check password
    if(data.pass === channels[data.id].lock || users[socket.id].hasPermission("noPassword"))
    {
        log("logs/info.log", users[socket.id].name + " switched from " + users[socket.id].getChannel().name + ":" + users[socket.id].getChannel().id + " to " + channels[data.id].name + ":" + data.id);
        if(channels[data.id].owner !== undefined) {
            socket.emit("exception", {"reason" : "Welkom in het tijdelijke kanaal: "+channels[data.id].name+", owner: "+channels[data.id].owner});
        }
        users[socket.id].getChannel().leaveChannel(socket.id);
        users[socket.id].channel = data.id;
        channels[data.id].joinChannel(socket.id);
    }else{
        socket.emit("exception", {"reason" : "Je probeerde het kanaal te joinen met een fout wachtwoord."});
    }
});