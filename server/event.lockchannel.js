/** LockChannel Event
 *
 * When a user sends the package 'LockChannel'
 * @data.id = the id of the channel, @data.pass =  the password
 *
 */
socket.on('lockChannel', function(data)
{
    var lock = true,
        pass = data.pass;

    if(data.id >= nextChannelId){
        socket.emit("exception", {"reason" : "Dit kanaal bestaat niet, heb je wel het juiste id gebruikt?"});
        return;
    }

    if(isNaN(data.id)) {
        socket.emit("exception", {"reason" : "Dit is geen id, stuur het id!"});
        return;
    }

    if(channels[data.id].owner != users[socket.id].name) {
        if(!users[socket.id].hasPermission("lock"))
        {
            socket.emit("exception", {"reason" : "Je hebt geen permissie voor dat commando."});
            return;
        }
    }

    if(data.pass === undefined)
    {
        pass = "";
        lock = false;
    }

    if(data.id === undefined)
    {
        socket.emit("exception", {"reason" : "Het kanaal-id is niet meegestuurd"});
        return;
    }

    if(channels[data.id] === undefined)
    {
        socket.emit("exception", {"reason" : "Het kanaal bestaat niet."});
        return;
    }

    log("logs/info.log", users[socket.id].name + " changed the lock of " + channels[data.id].name + ":" + data.id + " to " + lock);
    channels[data.id].lock = pass;
    io.sockets.emit("lockChannel", {"id": data.id, "locked": lock});

});