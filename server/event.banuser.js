/** Ban Event
 *
 * When a user sends the package 'ban'
 * @data.id = the id of the user
 *
 */
socket.on('ban', function(data)
{

    if(!users[socket.id].hasPermission("ban"))
    {
        socket.emit("exception", {"reason" : "Je hebt geen permissie voor dat commando."});
        return;
    }

    if(data.id === undefined)
    {
        socket.emit("exception", {"reason" : "Het user-id is niet meegestuurd"});
        return ;
    }

    Object.each(users, function(v, k){
        if(v.id == data.id){
            console.log("Banned: "+users[k].name);
            banned.push(data.id);
            log("logs/info.log",users[socket.id].name + " banned " + users[k].name + " (" + data.id + ")");
            v.socket.emit("exception", {"reason" : "Je bent gebanned."});
            v.socket.disconnect();
            return;
        }

    });

});