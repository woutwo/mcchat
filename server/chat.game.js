// @TODO: Globaal object met alle gemaakte games
// @TODO: Globaal object met alle spellen + informatie daarover
// @TODO: Communicatie met de spelservers en chat
// @TODO: Mogelijkheid spellen te starten/joinen via de chat
// @TODO: Spellen intergreren in de client
Game = function(id, game, numPlayers){

    this.id         = id;
    this.game       = game;
    this.numPlayers = numPlayers;
    this.users      = [];
    this.started    = false;

    // Voeg een gebruiker toe aan de lijst
    this.addUser  = function(socketId){

        if(this.users.length >= this.numPlayers){

            console.log("Dit spel zit reeds vol");
            return false;
        }

        this.users.push(socketId);

        if(this.users.length == this.numPlayers){

            this.start();
        }
    }

    // Verwijder een gebruiker uit de lijst
    this.removeUser = function(socketId){

        delete this.users[socketId];
    }

    // Start een game
    this.start      = function(){

        // Voorkom joinen
        this.started = true;

        // Maak nieuwe lobby-array
        var temp = [];

        // Dit voorkomt niet kloppende indexnummers wanneer iemand tijdens het joinen de lobby verlaat
        // Dan zou je een array krijgen met keys 1,3,4,5 en dat wordt nu 1,2,3,4
        Array.each(this.users, function(v){
            temp.push(v);
        });

        // Sla de nieuwe userlist op
        this.users = lobby;
    }
};