//(C) Jord Nijhuis, Niels van der Vegt and Wouter Mijnhart 2013

// @TODO: Bans e.d. in configuratiebestand opslaan
var banned  = new Array();
var muted   = new Array();
var mootools = require('mootools'),
    fs = require('fs'),
    http = require('http'),
    users = {},
    channels = {},
    nextChannelId = 0,
    vanished	  = [],
    versionNumber = "1.2.2";

// Laad config-bestand in
try{

    var config = eval("("+fs.readFileSync('config/config.json', 'utf8')+")");
    console.log('Configuratiebestand geladen!');

}catch(err){

    console.log("Configuratiebestand kan niet geladen worden!");
    throw new Error(err.message);
}

// Fancy opstartscherm
console.log('');
console.log('+++++++++++++++++++++++++++++++++++++++++++++++');
console.log('+++ CHAT SERVER                             +++');
console.log('+++++++++++++++++++++++++++++++++++++++++++++++');
console.log('VERSIE: '+versionNumber);
console.log('NAAM:   '+config.name);
console.log('PORT:   '+config.portChat);
console.log('API:    '+config.portApi);
console.log('');

// Start de socket.io-listener
var io = require('socket.io').listen(config.portChat);


io.enable('browser client minification');  // send minified client
io.enable('browser client etag');          // apply etag caching logic based on version number
io.enable('browser client gzip');          // gzip the file
io.set('log level', 1);                    // reduce logging
io.set('transports', [                     // enable all transports (optional if you want flashsocket)
        'websocket',
        'flashsocket',
        'htmlfile',
        'xhr-polling',
        'jsonp-polling'
]);

// Onze eigen importeur
// @TODO: Dit ooit eens fixen

/*loadModule = function(filename){

    console.log('Loading module '+filename);
    return eval(fs.readFileSync(filename, 'utf8'));
};
*/

// Include chat files
eval(fs.readFileSync('chat.channel.js', 'utf8'));
eval(fs.readFileSync('chat.user.js', 'utf8'));


function htmlEscape(text) {
   return text.replace(/&/g, '&amp;').
     replace(/</g, '&lt;').
     replace(/"/g, '&quot;').
     replace(/'/g, '&#039;');
}
 
function isNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n);
}

 
/** log
*
* Writes the given message to a file
* @path = The path of the file, @message = the message
*
*/
 
fs.exists = fs.exists || require('path').exists; //So the logger works on both version

//@TODO: Automatisch bestandjes maken voor de log

function log(path, message){

    //return;
	/*d = new Date();
    utc = d.getTime() + (d.getTimezoneOffset() * 60000);
    nd = new Date(utc + (3600000*2));
	var year = nd.getFullYear();
	var month = (nd.getMonth())+1;
	var day = nd.getDate();
    var dateAndTime = nd.toLocaleString();
	if(path == "logs/info.log") {
		if (!fso.FolderExists(year)) {
			fso.createFolder(year);
		}
		if (!fso.FolderExists(year+"\\"+month)) {
			fso.createFolder(year+"\\"+month);
		}
		if (fso.FileExists(year+"\\"+month+"\\"+day+".log")) {  
			var myFile = fso.CreateTextFile(year+"\\"+month+"\\"+day+".log");  
			myFile.Close();  
		}
		path = year+"\\"+month+"\\"+day+".log";
		fso = null;  
	}

    stream = fs.createWriteStream(path, {
        'flags': 'a+',
        'encoding': 'utf8',
        'mode': 0644
    });
 
	stream.write(dateAndTime + " ", 'utf8');
	stream.write(message + "\n", 'utf8');
	stream.end();*/
	
	//return;
	d = new Date();
    utc = d.getTime() + (d.getTimezoneOffset() * 60000);
    nd = new Date(utc + (3600000*2));
    var dateAndTime = nd.toLocaleString();

    stream = fs.createWriteStream(path, {
        'flags': 'a+',
        'encoding': 'utf8',
        'mode': 0644
    });
 
	stream.write(dateAndTime + " ", 'utf8');
	stream.write(message + "\n", 'utf8');
	stream.end();
}

function cleanUp()
{
        var foundUsers = {};
 
        io.sockets.clients().forEach(function (socket) {
 
                if(users[socket.id] !== undefined)
                {
                        foundUsers[socket.id] = users[socket.id];
                }
        });
 
        Object.each(users, function(v, k)
        {
                if(foundUsers[k] === undefined)
                {
                        console.log("User with socket-id" + k + "wasn't found in any lists while doing cleanup");
                        log("logs/info.log", "User with socket-id" + k + "wasn't found in any lists while doing cleanup");
 
                        io.sockets.socket(k).emit("exception", {"reason" : "Je was niet gevonden in de lijst." });
                        io.sockets.emit("removeUser", {"id" : k });
 
 
                        Object.each(channels, function(v1, k1)
                        {
 
                                Object.each(v1.userList, function(v2, k2)
                                {
                                        if(k2 === k)
                                        {
                                                v1.leaveChannel(k); //Let him leave the channel
                                        }
                                });
                        });

                        delete users[k];
                }
        });
 
}

// Channels toevoegen
console.log("Channels laden...");
Array.each(config.channels, function(v){

    if(v.length == 1){

        new Channel(v[0]);
    }else if(v.length == 2){

        new Channel(v[0], v[1]);
    }else if(v.length == 3){

        new Channel(v[0], v[1], v[2]);
    }

    console.log("+ "+v[0]);
});

setInterval(function(){
        cleanUp();
       
}, 300000);

//Yay a new client
io.sockets.on('connection', function(socket){
        //Check if the user is logged in after 5 seconds
        setTimeout(function(){
       
                if( users[ socket.id] === undefined && channels[0].isLocked() === false)//If he isn't
                {
                        socket.emit("exception", {"reason" : "Het duurde te lang om in te loggen." });
                        socket.disconnect();
                }
        }, 5000);

        eval(fs.readFileSync('event.login.js', 'utf8'));

        socket.emit('rootLocked', {'locked' : channels[0].isLocked() });

        eval(fs.readFileSync('event.me.js', 'utf8'));
        eval(fs.readFileSync('event.chat.js', 'utf8'));
        eval(fs.readFileSync('event.alert.js', 'utf8'));
        eval(fs.readFileSync('event.announce.js', 'utf8'));
        eval(fs.readFileSync('event.report.js', 'utf8'));

        eval(fs.readFileSync('event.moveuser.js', 'utf8'));
        eval(fs.readFileSync('event.kickuser.js', 'utf8'));
        eval(fs.readFileSync('event.banuser.js', 'utf8'));
        eval(fs.readFileSync('event.unbanuser.js', 'utf8'));
        eval(fs.readFileSync('event.muteuser.js', 'utf8'));
        eval(fs.readFileSync('event.unmuteuser.js', 'utf8'));

        eval(fs.readFileSync('event.addchannel.js', 'utf8'));
        eval(fs.readFileSync('event.changechannel.js', 'utf8'));
        eval(fs.readFileSync('event.lockchannel.js', 'utf8'));
        eval(fs.readFileSync('event.removechannel.js', 'utf8'));

        eval(fs.readFileSync('event.disconnect.js', 'utf8'));
});


/* WEBSERVER */
http.createServer(function (req, res) {
	
	var count = 0;
	res.writeHead(200, {'Content-Type': 'text/html'});

	Object.each(users, function(v, k){
	
		if(vanished.indexOf(v.id) === -1)
			count++;	
	});

	res.end('{"usersOnline": '+count+'}');
}).listen(config.portApi);
