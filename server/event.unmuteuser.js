/** Unmute Event
 *
 * When a user sends the package 'unmute'
 * @data.id = the id of the user
 *
 */
socket.on('unmute', function(data)
{

    if(!users[socket.id].hasPermission("mute"))
    {
        socket.emit("exception", {"reason" : "Je hebt geen permissie voor dat commando."});
        return;
    }

    if(data.id === undefined)
    {
        socket.emit("exception", {"reason" : "Het user-id is niet meegestuurd"});
        return ;
    }

    Object.each(users, function(v, k){
        if(v.id == data.id){
            if(muted.indexOf(data.id) != -1) {
                var index = muted.indexOf(data.id);
                muted.splice(index, 1);
                log("logs/info.log",users[socket.id].name + " unmuted " + data.id);
                console.log("Unmuted: "+data.id);
                socket.emit("exception", {"reason" : "De user is unmuted."});
                v.socket.emit("exception", {"reason" : "Je bent unmuted."});
                return;
            } else { socket.emit("exception", {"reason" : "De user staat niet in de mutelijst"}); return; }
        } else { socket.emit("exception", {"reason" : "De ids zijn niet hetzelfde?"+v.id+" | "+data.id}); return; }

    });

});