/** AddChannel Event
 *
 * When a user sends the package 'addChannel'
 * @data.name = The name of the channel, @data.parent = The Id of the parent
 *
 */
socket.on('addChannel', function(data)
{
    //Changed parent to data.parent, undo if needed.
    if(data.parent == undefined) {
        data.parent = 8;
    }
    if(data.parent != 8) {
        if(!users[socket.id].hasPermission("addChannel"))
        {
            socket.emit("exception", {"reason" : "Je hebt geen permissie voor dat commando."});
            return;
        }
    }

    //Check if name is undefined
    if(data.name === undefined)
    {
        socket.emit("exception", {"reason" : "De naam van het kanaal is niet meegestuurd."});
        return;
    }

    //Check if the name isn't empty
    if(data.name.length === 0)
    {
        socket.emit("exception", {"reason" : "De naam is leeg."});
        return;
    }

    if(data.name.length < 3)
    {
        socket.emit("exception", {"reason" : "De naam is te kort!"});
        return;
    }

    if(data.name.length > 20)
    {
        socket.emit("exception", {"reason" : "De naam is te lang!"});
        return;
    }

    //Check if parent exists
    var parent = data.parent;
    if(channels[data.parent] === undefined)
    {
        parent = users[socket.id].channel; //If user doesn't give a parent: The current channel is parent
    }

    if(parent< 0)
    {
        socket.emit("exception", {"reason" : "Je kan geen negatief getal gebruiken is parent."});
        return;
    }

    log("logs/info.log", users[socket.id].name + " created a new channel " + data.name + " with parent " + channels[parent].name + ":" + data.parent);

    new Channel(htmlEscape(data.name), parent, "", (parent == 8 ? true : false));

    users[socket.id].getChannel().leaveChannel(socket.id);
    channels[nextChannelId -1 ].joinChannel(socket.id);
    users[socket.id].channel = nextChannelId -1;

    if(data.parent == 8) {
        channels[nextChannelId -1 ].owner = users[socket.id].name;
    }

});