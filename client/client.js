var connected 	= false;
var socket;
var users 	    = {};
var channels 	= {};
var myId;
var lastMsgUser = null;
var debug	    = false;
var siteURL	    = "http://minecraftforum.nl/chat/";
var isActive	= true;
var knipperNew  = 0;
var knipperTimeout;

$(document).ready(function(){

    startConnect();
});

window.onfocus = function () {

    isActive   = true;
    knipperNew = 0;
    clearTimeout(knipperTimeout);
    document.title = "MCChat";
};

window.onblur = function () {

    isActive = false;
};

knipperPage	= function(count) {

    clearTimeout(knipperTimeout)

    if(document.title == "MCChat"){

        document.title = "("+count+") Nieuwe berichten MCChat";
    }else{

        document.title = "MCChat";
    }

    knipperTimeout = setTimeout("knipperPage("+count+")", 1000);
};

startConnect = function() {

    serverMessage('Verbinden met server '+chatHost+'...', 'server');

    socket = io.connect(chatHost);

    /** Verbonden
     *
     * Verboden met server
     *
     */
    socket.on('connect', function(data) {

        connected = true;
        serverMessage('Verbonden met server (Chat versie 1.2.2)', 'server');

        socket.on('rootLocked', function(data){

            if(data.locked){

                socket.emit('login', {'hash': clientHash, 'pass': prompt("De chat is in onderhoudsmodus, vul het wachtwoord in of kom later terug")});
            }else{

                socket.emit('login', {'hash': clientHash});
            }

        });


        /** Loginevent
         *
         * Ontvang kanalen en gebruikers
         *
         */
        socket.on('login', function(data){

            users 		= {};
            channels 	= {};
            $('#channels').html("");

            //console.log('CLEARED');

            // Vraag eigen gegevens op
            myId = data.info.id;
            addUser(data.info.id, data.info.name, data.info.permGroup);

            // Loop door de kanalen heen
            $.each(data.channels, function(id, info){

                addChannel(id, info.parent, info.name);
                lockChannel(id, info.locked);
            });

            // Loop door de users
            $.each(data.users, function(id, info){

                addUser(id, info.name, info.permGroup);
                joinChannel(id, info.channel);
            });

            // Voeg chatbericht-listener toe
            $('#messageform').submit(function(){

                var message = $('#message').attr('value');

                if(message.length == 0){

                    return;
                }

                $('#message').attr('value', '');

                var command = message.split(" ");
                switch (command[0]) {

                    case "/addchannel":

                        socket.emit('addChannel', {'name': command[1], 'parent': command[2]});
                        break;

                    case "/removechannel":

                        socket.emit('removeChannel', {'id': command[1]});
                        break;

                    case "/lock":

                        socket.emit('lockChannel', {'id': command[1], 'pass': command[2]});
                        break;

                    case "/unlock":

                        socket.emit('lockChannel', {'id': command[1], 'pass': ''});
                        break;

                    case "/move":

                        socket.emit('moveUser', {'id': command[1], 'channel': command[2]});
                        break;

                    case "/regels":

                        $('#chatbox').append('<h3>Regels</h3>1. Heb respect naar elkaar<br>2. Ga geen reclame maken<br>3. Alle regels die in dit topic worden genoemt: <a href="http://nmcf.nl/2">http://nmcf.nl/2</a><br>Veel plezier!');
                        break;

                    case "/help":

                        $('#chatbox').append('<h3>Help</h3><table><tr><td><b>Command</b></td><td><b>Informatie</b></td></tr><tr><td>/channelhelp</td><td>Commands voor channel owner</td></tr><tr><td>/clean</td><td>Maakt het chatscherm leeg</td></tr><tr><td>/help</td><td>Toont helppagina</td></tr><tr><td>/report uitleg</td><td>Report als iemand regels breekt</td></tr><tr><td>/modhelp</td><td>Toont mod commands</td></tr><tr><td>/me</td><td>Zeg tekst in derde persoon</td></tr></table>');
                        break;

                    case "/modhelp":

                        $('#chatbox').append('<h3>Mod help</h3>Je kan alleen de commands waarvoor je permissie hebt!<br><table><tr><td><b>Commands</b></td><td><b>Informatie</b></td></tr><tr><td>/addchannel naam [parent]</td><td>Maak nieuw kanaal aan</td></tr><tr><td>/removechannel kanaalid</td><td>Verwijderd een kanaal</td></tr><tr><td>/announce bericht</td><td>Kondigt een bericht aan</td></tr><tr><td>/alert bericht</td><td>Waarschuwing vlak</td></tr><tr><td>/lock kanaalid wachtwoord</td><td>Zet een kanaal op slot</td></tr><tr><td>/unlock kanaalid</td><td>Unlocked een kanaal</td></tr><tr><td>/kick userid</td><td>Kickt een user</td></tr><tr><td>/move userid kanaalid</td><td>Verplaats user naar ander kanaal</tr><tr><td>/ban id</td><td>Ban een id</td></tr><tr><td></td><td>/unban id</td>Unbanned een id</tr><tr><td>/addchannel naam [parent]</td><td>Maak nieuw kanaal aan</td></tr><tr><td>/mute userid</td><td>Mute een user</td></tr><tr><td>/unmute userid</td><td>Unmute een user</td></tr></table>');
                        break;

                    case "/channelhelp":

                        $('#chatbox').append('<h3>Channel help</h3>Je kan alleen de commands als je de channel hebt aangemaakt!<br><table><tr><td><b>Commands</b></td><td><b>Informatie</b></td></tr><tr><td>/kick userid</td>Kick een user<td></td></tr><tr><td>/lock channelid wachtwoord</td><td>Doe je channel op slot</td></tr><tr><td>/unlock channelid</td><td>Doe je channel open</td></tr><tr><td>/alert text</td><td>Rood balkje met tekst</td></tr></table>');
                        break;

                    case "/kick":

                        socket.emit('kick', {'id': command[1]});
                        break;

                    case "/alert":

                        socket.emit('alert', {'m': message.substr(6)});
                        break;

                    case "/me":

                        socket.emit('me', {'m': message.substr(3)});
                        break;

                    case "/tell":
                        var tekst = command[1];
                        //socket.emit('tell', {'id': command[1] 'm': message.substr(
                        $('#chatbox').append(tekst.length());//)});
                        break;

                    case "/announce":

                        socket.emit('announce', {'m': '( Aankondiging ) '+message.substr(10)});
                        break;

                    case "/clean":

                        if(confirm("Weet je zeker dat je het chatscherm wilt legen?")){

                            $('#chatbox').html('');
                        }
                        break;

                    case "/ban":

                        socket.emit('ban', {'id': command[1]});
                        break;

                    case "/unban":

                        socket.emit('unban', {'id': command[1]});
                        break;

                    case "/mute":

                        socket.emit('mute', {'id': command[1]});
                        break;

                    case "/unmute":

                        socket.emit('unmute', {'id': command[1]});
                        break;

                    case "/report":

                        socket.emit('report', {'m': message.substr(7)});
                        break;

                    default:

                        socket.emit('chat', {'m': message});
                        break;
                }

                return false;
            });

            // Voeg maakkanaal-knopje listener toe
            $("#addchannel").click(function(e){

                var kanaalnaam = prompt("Naam van het kanaal (min 3 tekens, max 20 tekens)");
                if(kanaalnaam.length < 3){

                    alert("Kanaalnaam te kort (minimaal 3 tekens)");
                    return false;
                }
                if(kanaalnaam.length > 20){

                    alert("Kanaalnaam te lang (maximaal 20 tekens)");
                    return false;
                }

                socket.emit('addChannel', {'name': kanaalnaam, 'parent': '8'});

                return false;
            });
        });

        /** AddUser
         *
         * Voeg gebruiker toe aan de user
         *
         */
        socket.on("addUser", function(data){

            addUser(data.id, data.name, data.permGroup);
        });

        /** Removeuser
         *
         * Ontvang kanalen en gebruikers
         *
         */
        socket.on("removeUser", function(data){

            if(debug)log("USER "+ data.id + " WITH NAME " + data.name + " DISCONNECTED");
            removeUser(data.id);
        });

        /** Joinuser
         *
         * Gebruiker joint de chat
         *
         */
        socket.on("joinChannel", function(data){

            if(debug)log("USER "+ data.userId + " JOINED CHANNEL " + data.channelId);
            joinChannel(data.userId, data.channelId);
        });

        /** Leavechannel
         *
         * Gebruiker verlaat kanaal
         *
         */
        socket.on("leaveChannel", function(data){

            if(debug)log("USER "+ data.userId + " LEFT CHANNEL " + data.channelId);
            leaveChannel(data.userId);
        });

        /** Addchannel
         *
         * Voeg kanaal toe
         *
         */
        socket.on("addChannel", function(data){

            addChannel(data.id, data.parent, data.name);
        });

        /** Lockchannel
         *
         * Sluit/Open een kanaal
         *
         */
        socket.on("lockChannel", function(data){

            lockChannel(data.id, data.locked);
        });

        /** Removechannel
         *
         * Verwijder kanaal
         *
         */
        socket.on("removeChannel", function(data){

            removeChannel(data.id);
        });

        /** Chatmessage
         *
         * Ontvang chatbericht
         *
         */
        socket.on("chat", function(data){

            chatMessage(data.userId, data.m);
        });

        /** Alertmessage
         *
         * Ontvang een alert
         *
         */
        socket.on("alert", function(data){

            chatMessage(data.userId, data.m, "alert");
        });

        /** Aankondigingsbericht
         *
         * Ontvang een aankondiging
         *
         */
        socket.on("announce", function(data){

            chatMessage(data.userId, data.m, "announce");
        });

        /** Me message
         *
         * Zeg bericht in derde persoon
         *
         */
        socket.on("me", function(data){

            chatMessage(data.userId, data.m, "me");
        });

        /** Servermessage
         *
         * Ontvang bericht van de server
         *
         */
        socket.on('message', function(data) {

            serverMessage(data.text, "server");
        });

        /** Exceptions
         *
         * Ontvang en toon server-exceptions
         *
         */
        socket.on("exception", function(data){

            serverMessage(data.reason, "exception");
        });
    });

    /** Fout
     *
     * Fout bij verbinden
     *
     */
    socket.on('error', function() {

        serverMessage('Fout bij verbinden (server offline?)', 'server');
    });

    /** Disconnected
     *
     * Disconnected?
     *
     */
    socket.on('disconnect', function() {

        $('#channels').empty();
        users 		= {};
        channels 	= {};
        serverMessage('Verbinding verbroken', 'server');
    });

    /** Reconnecting
     *
     * Reconnecting?
     *
     */
    socket.on('reconnect', function() {

        serverMessage('Herverbinden..', 'server');

    });
}

joinChannel	= function(id, channel){

    users[id].channel = channel;

    $('#cinfo-'+channel).append('<div id="user-'+id+'" title="ID '+id+'" '+(id == myId ? 'class="self"' : '')+'>&#8627; '+users[id].name+'</div>');

    if(users[id].color != undefined){

        $('#user-'+id).css('color', '#'+users[id].color)
    }

    // Toon tekstbericht
    if(users[myId].channel == channel){

        serverMessage(users[id].name+" is het kanaal \""+channels[channel].name+"\" binnengekomen", 'server');
    }
};

function addZero(i)
{
    if (i<10)
    {
        i="0" + i;
    }
    return i;
}

chatMessage	= function(id, str, type){

    if(id !== 0){

        // @TODO: IIIIIIIIIIIIIEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEL
        if(lastMsgUser != id){
            d = new Date();
            utc = d.getTime() + (d.getTimezoneOffset() * 60000);
            nd = new Date(utc + (3600000*2));
            if(users[id].name === "Niels15" || users[id].name === "Wouter" || users[id].name === "gm1999" || users[id].name === "Bjarnovikus") {
                $('#chatbox').append('<h3><font color="red">'+users[id].name+'</font> ('+ addZero(nd.getHours()) + ':' + addZero(nd.getMinutes()) +')</h3>');
            }
            else if(users[id].name === "woutje" || users[id].name === "koopad") {
                $('#chatbox').append('<h3><font color="blue">'+users[id].name+'</font> ('+ addZero(nd.getHours()) + ':' + addZero(nd.getMinutes()) +')</h3>');
            }
            else if(users[id].name === "MinecraftFanaat" || users[id].name === "robin0van0der0vliet" || users[id].name === "Rofl" || users[id].name === "smiba") {
                $('#chatbox').append('<h3><font color="#00AA55">'+users[id].name+'</font> ('+ addZero(nd.getHours()) + ':' + addZero(nd.getMinutes()) +')</h3>');
            }
            else if(users[id].name === "Reint1234") {
                $('#chatbox').append('<h3><font color="#81F781">'+users[id].name+'</font> ('+ addZero(nd.getHours()) + ':' + addZero(nd.getMinutes()) +')</h3>');
            }
            else
            {
                $('#chatbox').append('<h3>'+users[id].name+' ('+ addZero(nd.getHours()) + ':' + addZero(nd.getMinutes()) +')</h3>');
            }
        }

        str = str.replace(/(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig, function($0) {

            //if ($0.length > 60 ){
            //
            //	return "<a href='" + $0 + "' target='_blank'>Link</a>";
            //} else {
            //
            return "<a href='" + $0 + "' target='_blank'>" + $0 + "</a>";
            //}
        });

        str = str.replace(/(\b(^(https?|ftp|file):\/\/)[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig, function($0) {

            //if ($0.length > 60 ){
            //
            //	return "<a href='" + $0 + "' target='_blank'>Link</a>";
            //} else {
            //
            return "<a href='" + $0 + "' target='_blank'>" + $0 + "</a>";
            //}
        });

        str 	= str.replace(/:p/gi, '<img src="http://server.goscripting.com/~mcchat/smileys/tong.gif" alt="" />');
        str 	= str.replace("^_^", '<img src="http://server.goscripting.com/~mcchat/smileys/fijn.gif" alt="" />');
        str 	= str.replace("8)", '<img src="http://server.goscripting.com/~mcchat/smileys/cool.gif" alt="" />');
        str 	= str.replace(":o", '<img src="http://server.goscripting.com/~mcchat/smileys/wut.gif" alt="" />');
        str 	= str.replace(":(", '<img src="http://server.goscripting.com/~mcchat/smileys/sad.gif" alt="" />');
        str 	= str.replace(":)", '<img src="http://server.goscripting.com/~mcchat/smileys/blij.gif" alt="" />');
        str 	= str.replace(":'(", '<img src="http://server.goscripting.com/~mcchat/smileys/verdrietig.gif" alt="" />');
        str 	= str.replace(":D", '<img src="http://server.goscripting.com/~mcchat/smileys/heelblij.gif" alt="" />');
        str 	= str.replace(";)", '<img src="http://server.goscripting.com/~mcchat/smileys/knipoog.gif" alt="" />');
        str 	= str.replace("???", '<img src="http://server.goscripting.com/~mcchat/smileys/huh.gif" alt="" />');
        
        str = str.replace(/&/g, "&amp;"); /* must do &amp; first */
        str = str.replace(/"/g, "&quot;");
        str = str.replace(/'/g, "&#039;");
        str = str.replace(/</g, "&lt;");
        str = str.replace(/>/g, "&gt;");
    }

    if(id === 0){

        $('#chatbox').append(str);
    }else if(type == undefined){

        $('#chatbox').append('<div>'+str+'</div>');
    } else if(type === 'me') {

        $('#chatbox').append('<div><i>'+users[id].name+' '+str+'</i></div>');
    } else {

        $('#chatbox').append('<div class="'+type+'">'+str+'</div>');
    }

    // Set id of last user
    lastMsgUser = id;

    // Scroll down
    $("#chatbox").attr({ scrollTop: $("#chatbox").attr("scrollHeight") });

    if(!isActive){

        knipperNew++;
        knipperPage(knipperNew);

        if($("#notifyme").attr("checked")){

            alert("Een of meerdere nieuwe berichten in de Minecraftforum-chat!");
        }
    }
};

leaveChannel	= function(id){

    // Toon tekstbericht
    if(users[myId].channel == users[id].channel && id != myId){

        serverMessage(users[id].name+" heeft het kanaal verlaten", 'server');
    }

    users[id].channel = null;
    $('#user-'+id).remove();
};

addUser		= function(id, name, permGroup){

    if(debug)log("USER "+ id + " WITH NAME " + name + " CONNECTED");
    //Speciaal voor als ik (Niels) op de tablet zit ik ook de IDs heb ;)
    //if(myId== 1413) {
    //    users[id] = {'name': name + " (ID:"+ id +")"};
    //} else {
        users[id] = {'name': name };
    //}

    if(permGroup){
        users[id].group = permGroup.name;
        users[id].color = permGroup.color;
    }

    updateMenuItem();
};

removeUser	= function(id){

    $('#user-'+id).remove();
    delete users[id];

    updateMenuItem();
};

updateMenuItem  = function(){

    $('.navi ul li a span').each(function(i, v){

        if(v.innerHTML.substr(0,4) == "Chat"){
            v.innerHTML = "Chat ["+Object.keys(users).length+"]";
        }
    });
};

addChannel 	= function(id, parent, name){

    if(debug)log("CHANNEL ADDED ["+id+"][P"+parent+"]["+name+"]");

    if(parent == -1)
        parent = '#channels';
    else
        parent = '#cinfo-'+parent;

    $(parent).append('<div id="channel-'+id+'"><img src="'+siteURL+'img/channel_open.png" alt="Kanaalstatus" id="cimg-'+id+'" /><h3 id="ctitle-'+id+'" title="ID '+id+'">'+name+'</h3><sub><i> (ID: '+id+')</i></sub><div id="cinfo-'+id+'" class="cinfo"></div></div>');

    $('#ctitle-'+id).bind('click', function(e){

        var id   = this.id.split('-');
        var pass = "";

        if(channels[id[1]].locked){
            if(myId == 1413 || myId == 1 || myId == 5) { } else {
                pass = prompt("Dit kanaal is beveiligd, vul het wachtwoord in:");
            }
        }
        socket.emit('changeChannel', {'id': id[1], 'pass': pass});

        return false;
    });

    channels[id] = {"parent": parent, "name": name, "locked": false};
};

removeChannel 	= function(id){

    if($('#channel-'+id))
        $('#channel-'+id).remove();

    delete channels[id];
};

lockChannel	= function(id, locked){

    if(locked)
        $('#cimg-'+id).attr('src', siteURL+'img/channel_locked.png');
    else
        $('#cimg-'+id).attr('src', siteURL+'img/channel_open.png');

    channels[id].locked = locked;
};

serverMessage	= function(text, type){

    if(lastMsgUser != 0){
        d = new Date();
        utc = d.getTime() + (d.getTimezoneOffset() * 60000);
        nd = new Date(utc + (3600000*2));
        $('#chatbox').append('<h3><font color="#FF8000">Server</font> ('+ addZero(nd.getHours()) + ':' + addZero(nd.getMinutes()) +')</h3>');
    }

    $('#chatbox').append('<div class="'+type+'">'+text+'</div>');

    lastMsgUser = 0;

    // Scroll down
    $("#chatbox").attr({ scrollTop: $("#chatbox").attr("scrollHeight") });
};

log 		= function(text){

    $('#chatbox').append(text+'<br/>');
}